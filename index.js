const express = require('express')
const mongoose = require('mongoose')
const port = 4000

const userRoutes = require('./routes/userRoutes')
const app = express()

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

mongoose.set('strictQuery', true)
mongoose.connect(
  'mongodb+srv://admin:admin123@b248.ctuoffq.mongodb.net/prototype-Capstone-2?retryWrites=true&w=majority',
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
)

let pikachuDatabase = mongoose.connection
pikachuDatabase.on('error', console.error.bind(console, 'connection error'))
pikachuDatabase.once('open', () =>
  // prettier-ignore
  console.log(
`|               Hello World! 👋🌏               |
|     We are connected to pikachuDatabase 🥳💯  |
*===============================================*`
  )
)

app.use('/users', userRoutes)

app.listen(process.env.PORT || port, () =>
  // prettier-ignore
  console.log(
`*===============================================*
| API is now online via port │█║▌║▌║${process.env.PORT || port}║▌║▌║█│ |
*===============================================*`
  )
)
