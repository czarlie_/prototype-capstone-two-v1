const User = require('../models/User')
const bcrypt = require('bcrypt')
const auth = require('../auth')

module.exports.getAllUsers = () =>
  User.find({}).then((result, error) => (error ? error : result))

module.exports.registerUser = (reqBody) => {
  let newUser = new User({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    email: reqBody.email,
    password: bcrypt.hashSync(reqBody.password, 12),
    mobileNumber: reqBody.mobileNumber,
    isAdmin: reqBody.isAdmin
  })
  return newUser.save().then((newUser, error) => (error ? false : true))
}

module.exports.loginUser = (reqBody) =>
  User.findOne({ email: reqBody.email }).then((result, error) => {
    if (result == null) return false
    else {
      let isPasswordSame = bcrypt.compareSync(reqBody.password, result.password)

      return isPasswordSame ? { access: auth.createAccessToken(result) } : false
    }
  })

module.exports.checkEmail = (reqBody) =>
  User.find({ email: reqBody.email }).then((result) =>
    result.length > 0
      ? `Email is already registered 😇`
      : `Email is not yet registered 🙁`
  )

module.exports.updateAdminStatus = (reqParamsId) => {
  return User.findById(reqParamsId).then((userResult) => {
    // prettier-ignore
    if (userResult.isAdmin) 
      userResult.isAdmin = false
    else
      userResult.isAdmin = true

    return userResult
      .save()
      .then((updatedStatus, error) => (error ? error : updatedStatus))
  })
}
