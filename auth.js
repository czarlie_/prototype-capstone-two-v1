const jwt = require('jsonwebtoken')
const secret = 'Cardo_Dalisay_Loves_Pikachu'

module.exports.createAccessToken = (userResult) => {
  let userData = {
    id: userResult._id,
    email: userResult.email,
    isAdmin: userResult.isAdmin
  }
  return jwt.sign(userData, secret, {})
}
