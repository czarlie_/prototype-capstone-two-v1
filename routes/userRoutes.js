const express = require('express')
const router = express.Router()
const userController = require('../controllers/userControllers')
// const auth = require('../auth')

// get all users
router.get('/all', (req, res) => {
  userController
    .getAllUsers()
    .then((resultFromUserController) => res.send(resultFromUserController))
})

// register user
router.post('/register', (req, res) => {
  userController
    .registerUser(req.body)
    .then((resultFromUserController) => res.send(resultFromUserController))
})

// authenticate user
router.post('/login', (req, res) => {
  userController
    .loginUser(req.body)
    .then((resultFromUserController) => res.send(resultFromUserController))
})

// check email
router.get('/checkEmailExists', (req, res) => {
  userController
    .checkEmail(req.body)
    .then((resultFromUserController) => res.send(resultFromUserController))
})

// change isAdmin status
router.put('/updateAdmin/:id', (req, res) => {
  userController
    .updateAdminStatus(req.params.id)
    .then((resultFromUserController) => res.send(resultFromUserController))
})

module.exports = router
