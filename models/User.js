const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    minLength: 1,
    maxLength: 12,
    required: [true, `First name is required! 🫠`]
  },
  lastName: {
    type: String,
    minLength: 1,
    maxLength: 12,
    required: [true, `Last name is required! 🫠`]
  },
  email: {
    type: String,
    minLength: 5,
    maxLength: 30,
    lowercase: true,
    required: [true, `Email is required! 😠`]
  },
  password: {
    type: String,
    minLength: 5,
    required: [true, `Password is required! 😠`]
  },
  mobileNumber: {
    type: Number,
    min: 10,
    required: [true, `Mobile Number is required! 🫠`]
  },
  isAdmin: {
    type: Boolean,
    default: false
  }
})

module.exports = mongoose.model('User', userSchema)
